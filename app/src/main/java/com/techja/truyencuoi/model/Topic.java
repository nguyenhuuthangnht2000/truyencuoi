package com.techja.truyencuoi.model;

import androidx.annotation.NonNull;

public class Topic {
    private final String name;
    private final String imagePath;

    public Topic(String name, String imagePath) {
        this.name = name;
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getName() {
        return name;
    }

    @NonNull
    @Override
    public String toString() {
        return "Topic{" +
                "name='" + name + '\'' +
                ", imagePath='" + imagePath + '\'' +
                '}';
    }
}
