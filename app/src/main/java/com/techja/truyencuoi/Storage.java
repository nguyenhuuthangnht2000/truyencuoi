package com.techja.truyencuoi;

import androidx.lifecycle.MutableLiveData;

import com.techja.truyencuoi.model.Story;
import com.techja.truyencuoi.model.Topic;

import java.util.List;

public class Storage {
    public final MutableLiveData<Integer> indexStory = new MutableLiveData<>();
    public Story currentStory;
    public List<Story> listStory;
    public Topic topic;
}
