package com.techja.truyencuoi.viewmodel;

import android.util.Log;

import com.techja.truyencuoi.model.Story;
import com.techja.truyencuoi.model.Topic;
import com.techja.truyencuoi.view.base.BaseViewModel;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class M001ViewModel extends BaseViewModel {
    private static final String TAG = M001ViewModel.class.getName();
    private List<Topic> topics;
    private List<Story> stories;


    public List<Topic> getTopics() {
        return topics;
    }

    public void initTopicList(String[] listPath) {
        topics = new ArrayList<>();
        for (String path : listPath) {
            String ten;
            String name = path.substring(1, path.indexOf("."));
            String name1 = path.substring(0, 1).toUpperCase();
            topics.add(new Topic(ten = name1 + name, "image/" + path));
        }
        Log.i(TAG, topics.toString());
    }

    public List<Story> getStories() {
        return stories;
    }

    public void initListStory(InputStream in) {

        stories = new ArrayList<>();
        try (InputStreamReader isr = new InputStreamReader(in, StandardCharsets.UTF_8);
             BufferedReader reader = new BufferedReader(isr)
        ) {

            String str;
            String name = null;
            StringBuilder content = new StringBuilder();
            while ((str = reader.readLine()) != null) {
                if (name == null) {
                    name = str;
                } else if (!str.contains("','0');")) {
                    content.append(str).append("\n");
                } else {
                    stories.add(new Story(name, content.toString()));
                    name = null;
                    content = new StringBuilder();
                }
            }
            reader.close();
            isr.close();
            in.close();
            Log.i(TAG, "ListStory " + stories.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
