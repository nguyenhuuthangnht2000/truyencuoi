package com.techja.truyencuoi.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.techja.truyencuoi.R;
import com.techja.truyencuoi.model.Story;

import java.util.List;

public class DetailStoryAdapter extends PagerAdapter {
    private final List<Story> listStory;
    private final Context mContext;

    public DetailStoryAdapter(List<Story> listStory, Context mContext) {
        this.listStory = listStory;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return listStory.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup viewPager, int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_detail_story, viewPager, false);

        TextView tvName = view.findViewById(R.id.tv_name);
        TextView tvContent = view.findViewById(R.id.tv_content);
        Story story = listStory.get(position);

        tvName.setText(story.getName());
        tvContent.setText(story.getContent());
        tvName.setTag(story);

        viewPager.addView(view);
        return view;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
