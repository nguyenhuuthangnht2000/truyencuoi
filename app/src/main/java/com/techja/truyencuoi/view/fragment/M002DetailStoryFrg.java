package com.techja.truyencuoi.view.fragment;

import android.view.View;

import androidx.viewpager.widget.ViewPager;

import com.techja.truyencuoi.App;
import com.techja.truyencuoi.CommonUtils;
import com.techja.truyencuoi.R;
import com.techja.truyencuoi.Storage;
import com.techja.truyencuoi.databinding.FrgM002DetailStoryBinding;
import com.techja.truyencuoi.view.activity.MainActivity;
import com.techja.truyencuoi.view.adapter.DetailStoryAdapter;
import com.techja.truyencuoi.view.base.BaseFragment;
import com.techja.truyencuoi.viewmodel.M002ViewModel;

public class M002DetailStoryFrg extends BaseFragment<FrgM002DetailStoryBinding, M002ViewModel> {
    public static final String TAG = M002DetailStoryFrg.class.getName();
    public static final String KEY_STORY_INDEX = "KEY_STORY";

    @Override
    protected void initViews() {
        DetailStoryAdapter adapter = new DetailStoryAdapter(getStore().listStory, mContext);
        mBinding.vpStory.setAdapter(adapter);
        mBinding.actionBarDetail.tvTopic.setText(getStore().topic.getName());
        mBinding.actionBarDetail.ivBack.setOnClickListener(view -> ((MainActivity) mContext).onBackPressed());

        int index = getStore().listStory.indexOf(getStore().currentStory);
        mBinding.vpStory.setCurrentItem(index, true);

        //save topic to preference
        CommonUtils.getInstance().savePref(KEY_STORY_INDEX, index + "");

        final int size = getStore().listStory.size();

        getStore().indexStory.observe(this, position -> mBinding.actionBarDetail.tvIndex.setText(String.format("%s%s/%s", (position + 1) < 10 ? "0" : "", position + 1, size)));

        getStore().indexStory.setValue(index);

        mBinding.vpStory.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                getStore().indexStory.setValue(position);
                getStore().currentStory = getStore().listStory.get(position);
                CommonUtils.getInstance().savePref(KEY_STORY_INDEX, position + "");
            }
        });
    }

    private Storage getStore() {
        return App.getInstance().getStorage();
    }

    @Override
    protected Class<M002ViewModel> initViewModel() {
        return M002ViewModel.class;
    }

    @Override
    protected FrgM002DetailStoryBinding initViewBinding(View view) {
        return FrgM002DetailStoryBinding.bind(view);
    }

    @Override
    protected int getLayOutId() {
        return R.layout.frg_m002_detail_story;
    }
}
