package com.techja.truyencuoi.view.fragment;

import android.annotation.SuppressLint;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.techja.truyencuoi.App;
import com.techja.truyencuoi.CommonUtils;
import com.techja.truyencuoi.R;
import com.techja.truyencuoi.databinding.FrgM001MenuBinding;
import com.techja.truyencuoi.model.Story;
import com.techja.truyencuoi.model.Topic;
import com.techja.truyencuoi.view.activity.MainActivity;
import com.techja.truyencuoi.view.adapter.StoryAdapter;
import com.techja.truyencuoi.view.base.BaseFragment;
import com.techja.truyencuoi.viewmodel.M001ViewModel;

import java.io.IOException;
import java.io.InputStream;

public class M001MenuFrg extends BaseFragment<FrgM001MenuBinding, M001ViewModel> {
    public static final String TAG = M001MenuFrg.class.getName();
    private static final int LEVEL_OPEN = 1;
    private static final int LEVEL_CLOSE = 0;
    private static final String KEY_TOPIC = "KEY_TOPIC";


    @Override
    protected void initViews() {
        mBinding.actionBarMenu.ivMenu.setOnClickListener(this);
        mBinding.drawer.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                mBinding.actionBarMenu.ivMenu.setImageLevel(LEVEL_OPEN);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                mBinding.actionBarMenu.ivMenu.setImageLevel(LEVEL_CLOSE);
            }
        });
        initTopicList();
        // khởi tạo danh sách truyện của topic 0
        initListStory(App.getInstance().getStorage().topic);

        App.getInstance().getStorage().indexStory.observe(this, index -> mBinding.actionBarMenu.tvIndex.setText(String.format("%s%s/%s", (index + 1) < 10 ? "0" : "", index + 1, mViewModel.getStories().size())));

        int index = 0;
        if (App.getInstance().getStorage().indexStory.getValue() != null) {
            index = App.getInstance().getStorage().indexStory.getValue();
        }

        initIndexText(index);

    }

    private void initIndexText(int index) {
        mBinding.actionBarMenu.tvIndex.setText(String.format("%s%s/%s", (index + 1) < 10 ? "0" : "", index + 1, mViewModel.getStories().size()));
    }

    @Override
    protected void clickView(View view) {
        if (view.getId() == R.id.iv_menu) {
            openDrawer();
        }
    }

    private void openDrawer() {
        if (mBinding.actionBarMenu.ivMenu.getDrawable().getLevel() == LEVEL_OPEN) {
            mBinding.drawer.closeDrawer(GravityCompat.START);
        } else {
            mBinding.drawer.openDrawer(GravityCompat.START);
        }
    }

    private void initTopicList() {

        try {
            String[] listPath = mContext.getAssets().list("image");
            mViewModel.initTopicList(listPath);

            String topicImg = CommonUtils.getInstance().getPref(KEY_TOPIC);

            mBinding.includeMenu.lnTopic.removeAllViews();
            for (Topic topic : mViewModel.getTopics()) {
                @SuppressLint("InflateParams") View view = LayoutInflater.from(mContext).inflate(R.layout.item_topic, null);
                TextView tvTopic = view.findViewById(R.id.tv_topic);
                ImageView ivTopic = view.findViewById(R.id.iv_topic);

                if (topicImg != null && topicImg.equals(topic.getImagePath())) {
                    App.getInstance().getStorage().topic = topic;
                }

                tvTopic.setText(topic.getName());
                ivTopic.setImageBitmap(BitmapFactory.decodeStream(mContext.getAssets().open(topic.getImagePath())));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.bottomMargin = 30;
                mBinding.includeMenu.lnTopic.addView(view, params);
                //đính vào để dùng
                view.setTag(topic);

                view.setOnClickListener(M001MenuFrg.this::clickItem);
            }
            if (App.getInstance().getStorage().topic == null) {
                App.getInstance().getStorage().topic = mViewModel.getTopics().get(0);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void clickItem(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(mContext, androidx.appcompat.R.anim.abc_fade_in));
        Topic topic = (Topic) view.getTag();

        Toast.makeText(mContext, topic.getName(), Toast.LENGTH_LONG).show();

        mBinding.drawer.closeDrawer(GravityCompat.START);

        CommonUtils.getInstance().savePref(M002DetailStoryFrg.KEY_STORY_INDEX, "0");
        initIndexText(0);
        initListStory(topic);
    }

    private void initListStory(Topic topic) {

        if (App.getInstance().getStorage().topic == null) {
            App.getInstance().getStorage().topic = topic;
        }

        CommonUtils.getInstance().savePref(KEY_TOPIC, topic.getImagePath());

        mBinding.actionBarMenu.tvTopic.setText(topic.getName());

        try {
            InputStream in = mContext.getAssets().open("story/" + topic.getName() + ".txt");
            mViewModel.initListStory(in);

            //Restore story and index
            String index = CommonUtils.getInstance().getPref(M002DetailStoryFrg.KEY_STORY_INDEX);
            if (index != null) {
                int intIndex = Integer.parseInt(index);
                App.getInstance().getStorage().currentStory = mViewModel.getStories().get(intIndex);
                App.getInstance().getStorage().indexStory.setValue(intIndex);
            }

            // đổ adapter RecyclerView
            StoryAdapter adapter = new StoryAdapter(mViewModel.getStories(), mContext, view -> M001MenuFrg.this.goDetailStory((Story) view.getTag()));

            mBinding.rvStory.setAdapter(adapter);

            mBinding.rvStory.scrollToPosition(App.getInstance().getStorage().indexStory.getValue() == null ? 0 : App.getInstance().getStorage().indexStory.getValue());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void goDetailStory(Story story) {

        App.getInstance().getStorage().listStory = mViewModel.getStories();
        App.getInstance().getStorage().currentStory = story;

        ((MainActivity) mContext).showFrg(M002DetailStoryFrg.TAG, true);
    }

    @Override
    protected Class<M001ViewModel> initViewModel() {
        return M001ViewModel.class;
    }

    @Override
    protected FrgM001MenuBinding initViewBinding(View view) {
        return FrgM001MenuBinding.bind(view);
    }

    @Override
    protected int getLayOutId() {
        return R.layout.frg_m001_menu;
    }
}
