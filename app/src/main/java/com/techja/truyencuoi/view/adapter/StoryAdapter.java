package com.techja.truyencuoi.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.techja.truyencuoi.App;
import com.techja.truyencuoi.R;
import com.techja.truyencuoi.model.Story;

import java.util.List;

public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.StoryHolder> {
    private final List<Story> listStory;
    private final Context mContext;
    private final View.OnClickListener event;


    public StoryAdapter(List<Story> listStory, Context mContext, View.OnClickListener event) {
        this.listStory = listStory;
        this.mContext = mContext;
        this.event = event;
    }


    @NonNull
    @Override
    public StoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_story, parent, false);

        return new StoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StoryAdapter.StoryHolder holder, int position) {
        Story story = listStory.get(position);
        if (story.equals(App.getInstance().getStorage().currentStory)) {
            holder.lnStory.setBackgroundResource(R.color.grayLight);
        } else {
            holder.lnStory.setBackgroundResource(R.color.white);
        }
        holder.tvName.setText(story.getName());

        holder.tvName.setTag(story);
    }

    @Override
    public int getItemCount() {
        return listStory.size();
    }


    public class StoryHolder extends RecyclerView.ViewHolder {
        LinearLayout lnStory;
        TextView tvName;

        public StoryHolder(View itemView) {
            super(itemView);

            lnStory = itemView.findViewById(R.id.ln_story);

            tvName = itemView.findViewById(R.id.tv_name);
            tvName.setOnClickListener(view -> {
                tvName.startAnimation(AnimationUtils.loadAnimation(mContext, androidx.appcompat.R.anim.abc_fade_in));
                event.onClick(tvName);
            });
        }
    }
}

