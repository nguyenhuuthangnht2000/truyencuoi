package com.techja.truyencuoi.view.fragment;

import android.os.Handler;
import android.view.View;

import com.techja.truyencuoi.R;
import com.techja.truyencuoi.databinding.FrgM000SplashBinding;
import com.techja.truyencuoi.view.activity.MainActivity;
import com.techja.truyencuoi.view.base.BaseFragment;
import com.techja.truyencuoi.viewmodel.M000ViewModel;

public class M000SplashFrg extends BaseFragment<FrgM000SplashBinding, M000ViewModel> {
    public static final String TAG = M000SplashFrg.class.getName();


    @Override
    protected void initViews() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ((MainActivity) mContext).showFrg(M001MenuFrg.TAG);
            }
        }, 2000);
    }

    @Override
    protected Class<M000ViewModel> initViewModel() {
        return M000ViewModel.class;
    }

    @Override
    protected FrgM000SplashBinding initViewBinding(View view) {
        return FrgM000SplashBinding.bind(view);
    }

    @Override
    protected int getLayOutId() {
        return R.layout.frg_m000_splash;
    }
}
